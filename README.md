# MachineShell
A Forth-Like Telnet interface for Jura coffee machines.

Implements a minimal Forth interpreter on an ESP8266 to control a Jura
coffee machine.

## Installation
Copy `include/example-config.h` to `include/config.h`.
Fill out the config variables.
Build with PlatformIO, upload to a D1 Mini.
Connect the coffee machine to the D1 Mini via a level shifter.
Connect to the D1 Mini with `telnet` or `netcat`.

## Usage
The MachineShell presents you with a very basic Forth-like interpreter,
without compilation features.
It is implemented using
[ForthyShell](https://github.com/alex-fu27/arduino-ForthyShell).

### Examples
Compute `1 + 3` and print:
```
1 3 + .
```

Brew a Flat White, maximum strength, hot, setting some amount values I
cant translate to milliliters.
```
2E product c!
0A strength c!
20 amount c!
20 milkAmount c!
02 temperature c!
brew
```

Forgot what you configured?
Output it:
```
coffee.
```

## Commands
If no stack effect is given in parentheses, the stack effect is `--`.

### General Commands
| Command | Description |
| ------- | ----------- |
| `flags.` | Print last flag field sent by machine. |

### Handshake Commands
| Command | Description |
| ------- | ----------- |
| `hstart` | Starts a handshake with the machine. |
| `ohstart` | Starts an obfuscated handshake with the machine. |
| `hstate@ ( -- flag )` | Returns 1 if the handshake has finished, 0 otherwise |

### Message sending commands
| Command | Description |
| ------- | ----------- |
| `%` | Read string until end of line. Send string to machine. |
| `$` | Read string until end of line. Send string to machine, with obfuscation. |

### Coffee brewing commands
| Command | Description |
| ------- | ----------- |
| `default` | Resets the coffee configuration to a reasonable default. |
| `coffee.` | Prints the coffee configuration as hex bytes. |
| `brew` | Send the `@TP` command, start brewing coffee. |

## Variables
The stack effect is `( -- addr )`.
If the type of the variable is `c`, must be fetched/stored using `c@` / `c!`.

### General variables
| Variable | Type | Description |
| -------- | ---- | ----------- |
| `showflags` | `i` | 2: Show flags as debug when received as message from machine. |
|             |     | 1: Show flags only if they changed.  |
|             |     | 0: Do not show flags. |

### Coffee variables
These variables are fields of a `Coffee` object.
They can be set to configure a coffee, then the command `brew` will brew
the configured coffee.

| Variable | Type | Description |
| -------- | ---- | ----------- |
| `product` | `c` | Product code |
| `grind` | `c` | Fineness of grinding |
| `strength` | `c` | Strength |
| `amount` | `c` | Amount of coffee |
| `milkAmount` | `c` | Amount of milk |
| `foamAmount` | `c` | Amount of milk foam |
| `temperature` | `c` | Temperature of coffee |
| `strokes` | `c` | Strokes for pot |
| `f09` | `c` | Field F09, is always `01` |
| `bypassAmount` | `c` | Bypass amount |
| `pause` | `c` | Milk pause time |
| `f12` | `c` | Field F12 |
| `f13` | `c` | Field F13 |
| `temperatures` | `c` | Milk/Foam temperature field |



