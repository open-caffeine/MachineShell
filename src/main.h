#ifndef MAIN_H
#define MAIN_H

#include <JuraCoffeeMachine.h>

extern JuraCoffeeMachine machine;

void setupTelnet();
void updateTelnet();

void setupOTAUpdate();
void updateOTAUpdate();

using PrintCallback = void (*)(const char*);
void prompt(PrintCallback pc);
void executePromptLine(const char* line, PrintCallback pc);

extern int debugShowFlags;

#endif

